from typing import List

from fastapi import APIRouter
from fastapi import status 
from fastapi import Depends 
from fastapi import HTTPException
from fastapi import Response


from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from models.cadastro_model import CadastroModel
from schemas.cadastro_schema import CadastroSchema
from core.deps import get_session
router = APIRouter()





# POST cadastro
@router.post('/', status_code=status.HTTP_201_CREATED, response_model=CadastroSchema)
async def post_cadastro(cadastro: CadastroSchema, db: AsyncSession = Depends(get_session)):
    novo_cadastro = CadastroModel(nome=cadastro.nome,
                            email=cadastro.email,
                            endereco=cadastro.endereco)


    db.add(novo_cadastro)
    await db.commit()

    return novo_cadastro

